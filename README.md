# Intégration de spip-cli et checkout à DDEV

DDEV est une couche d'abstraction à docker et docker-compose qui permet de mettre en place 
rapidement des environnements de développement avec de multiples versions de PHP, de Mysql 
ou MariaDb, avec Apache ou Nginx, et sans avoir rien installer ou modifier sur la machine hôte.

https://ddev.readthedocs.io

Ce script permet d'intégrer [spip-cli](https://contrib.spip.net/SPIP-Cli) et [checkout](https://git.spip.net/spip-contrib-outils/checkout) parmi les outils disponibles 
(comme composer, drush, artisan, wp-cli), pour les installer dans le conteneur et interagir 
avec le SPIP qui y est installé.

Ils fonctionneront alors avec la version de PHP du conteneur, et pas avec celle de la machine 
hôte.

Quelques liens de documentation :

1. [Installation](docs/10-INSTALL.md)
2. [SPIPer avec DDEV](docs/20-SPIP_DDEV.md)
3. [Utiliser ses clés SSH](docs/30-SSH.md)