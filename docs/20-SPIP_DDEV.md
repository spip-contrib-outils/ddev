# Utilisation de SPIP avec DDEV

Bricebou a écrit deux articles qui expliquent comment installer un SPIP dans un conteneur DDEV : 

[SPIPer avec DDEV](https://momh.fr/spiper-avec-ddev)

[SPIPer avec DDEV bis](https://momh.fr/spiper-avec-ddev-bis)
