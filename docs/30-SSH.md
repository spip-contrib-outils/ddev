# Gestion des clés SSH avec DDEV

Pour cloner depuis git.spip.net puis pusher ses PR, et si bien sûr vous y avez enregistré vos clés, 
il est plus pratique d'utiliser SSH (c'est à dire, commencer par cloner sous la forme 
`git clone git@git.spip.net:...`)

**Problème :** le conteneur DDEV (et donc, docker), ne connait pas vos clés ssh, celles de l'hôte.

Il suffit simplement de lancer `ddev auth ssh` dans le terminal de votre conteneur DDEV 
et de saisir votre passphrase, pour que le conteneur puisse interagir avec vos clés ssh, 
qui seront chargées dans l'agent en mémoire.  
Ce sera effectif jusqu'au prochain reboot.

**Nouveau problème :** parfois on a de nombreuses paires de clés ssh.

Cette discussion propose deux façons de contourner le problème :  
https://stackoverflow.com/questions/61485061/when-i-try-to-use-ssh-in-ddev-web-container-after-ddev-auth-ssh-the-ssh-keys