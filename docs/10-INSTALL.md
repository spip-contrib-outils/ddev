# Installation de spip-cli et checkout dans DDEV

## Méthode 1

- avantage: spip-cli est toujours à jour
- inconvénient: il est réinstallé à chaque `ddev restart` (mais ça prend
  2 secondes)

### 1/ Installation

Copier les fichiers `spip` et `checkout` qui sont à la racine de ce dépôt
dans `~/.ddev/commands/web`

(ou bien, pour une utilisation pour un projet en particulier, les copier
simplement dans le projet : `/var/www/html/nom_du_site/.ddev/commands/web`)

Puis redémarrer sur un site, pour pouvoir les utiliser, redémarrez le
simplement : `ddev restart nom_du_site`.

Si tout est ok, vous devez les voir dans la liste des commandes disponibles
avec `ddev -h`.

### 2/ Utilisation

Il suffit de lancer une première fois `ddev spip` ou  `ddev checkout`, qui vous
proposera de les installer, puis de les utiliser avec les commandes habituelles,
par exemple : `ddev spip dl` ou `ddev checkout spip .`

En cas de restart du conteneur (ou de redémarrage de l'hôte), les fichiers
système des conteneurs sont écrasés, la réinstallation de `spip-cli` se
relancera automatiquement à la première utilisation.

## Méthode 2

- inconvénient: il faut penser à mettre à jour (git pull) le spip-cli (global ou
  local) de temps en temps.

Il est possible d’installer spip-cli & checkout globalement pour chaque site
(la même version pour tous)ou de l’installer spécifiquement sur certains sites.

Il faut simplement se placer dans le bon répertoire `.ddev` (global ou
spécifique au site)

### Installation globale

- avantage: présent par défaut sur chaque projet
- dans les commandes suivantes, utiliser `~/.ddev` (le répertoire ddev global)

### Installation locale (par site)

- dans les commandes suivantes, remplacer `~/.ddev` par `{votre projet}/.ddev`
  (le ddev spécifique au site)

### spip-cli

```shell
cd ~/.ddev/homeadditions
git clone git@git.spip.net:spip-contrib-outils/spip-cli.git
cd spip-cli
composer install --no-dev
```

```shell
cd ~/.ddev/commands/web
echo '#!/bin/bash' > spip
echo '~/spip-cli/bin/spip "$@"' >> spip
```

Éditer (créer au besoin) `~/.ddev/homeadditions/.bash_aliases`

```shell
if [ ! -L /usr/local/bin/spip ]; then
    ln -s ~/spip-cli/bin/spip /usr/local/bin
fi
source ~/spip-cli/bin/spip_console_autocomplete
```

#### Mise à jour

```shell
cd ~/.ddev/homeadditions/spip-cli
git pull
composer install --no-dev
```

### checkout

Piège : DDEV n'aime pas trop les liens symboliques…

```shell
cd ~/.ddev/homeadditions
git clone git@git.spip.net:spip-contrib-outils/checkout.git
# DDEV aime pas le lien symbolique checkout -> checkout.php
rm checkout/checkout
```

```shell
cd ~/.ddev/commands/web
echo '#!/bin/bash' > checkout
echo '~/checkout/checkout.php "$@"' >> checkout
```

Éditer (créer au besoin) `~/.ddev/homeadditions/.bash_aliases`

```shell
if [ ! -L /usr/local/bin/checkout ]; then
    ln -s ~/checkout/checkout /usr/local/bin
fi
```

#### Mise à jour

```shell
cd ~/.ddev/homeadditions/checkout
git stash
git pull
git stash pop
composer install --no-dev
```

## Notes

### Cas où le `docroot` n'est pas à la racine

Si pour un projet donné le docroot n'est pas à la racine. Par exemple avec cette
ligne dans `.ddev/config.yaml`

```yaml
docroot: "web"
```

Alors il faut aussi configurer DDEV pour que les commands `web` (dont `spip`)
soit executées dans le bon dossier, sinon `spip-cli` ne trouvera pas
l'installation de spip.

Et donc ajouter dans votre `.ddev/config.yaml`

```yaml
working_dir:
  web: /var/www/html/web
```
